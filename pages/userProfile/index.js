import React, { useContext, useEffect, useState } from 'react';
import AppHelper from '../../app-helper';



export default function UserProfile() {

    const [data, setData] = useState('');


    useEffect(() => {
            const options = {
                headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
            }

            fetch(`${ AppHelper.API_URL }/users/details`, options)
            .then(AppHelper.toJSON)
            .then((data) => {

                setData(data)
            })

        }, [])



    return (

        <React.Fragment>
            <div className="text-center row vh-100">
                <div className="col-sm-12 my-auto">

                    <h3 className="">Name: {data.firstName} {data.lastName}</h3>
                    <h5>Email: {data.email}</h5>
                    <h5>Mobile Number: {data.mobileNo}</h5>
                </div>               
            </div>
        </React.Fragment>

    )
}




