import { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';


export default function IncomeBarChart({ incomeRecords }){

	// console.log(recordsData);

	const [months, setMonths] = useState(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]);
	const [monthlyIncome, setMonthlyIncome] = useState([]);

	useEffect(() => {

		setMonthlyIncome(months.map(month => {
		
				let income = 0;
	
				incomeRecords.forEach(element => {
	
					if(moment(element.createdOn).format('MMMM') === month){
						income += element.amount;
					}
	
				})
	
				return income;
	
			})
		)
	}, [incomeRecords])

	// console.log(monthlyExpenses)

	const data = {
		labels: months,
		datasets: [{
			label: 'Monthly Income for the year 2021',
			backgroundColor: 'rgba(255, 99, 132, 0.2)',
			borderColor: 'rgba(255, 99, 132, 1)',
			borderWidth: 1,
			hoverBackgroundColor: 'rgba(255, 99, 132, 0.4)',
			hoverBorderColor: 'rgba(255, 99, 132, 1)',
			data: monthlyIncome
		}]
	}

	return(
		<Bar data={data}/>
	)
}