import PropTypes from 'prop-types';
import { Card, Button } from 'react-bootstrap';
import { useState, useContext, useEffect } from 'react';

export default function Account() {

    // Use the UserContext and destructure it to access the user state defined in the App component
    const [account, setAccount] = useState('');
    const [details, setDetails] = useState('');
    const [cost, setCost] = useState(0);
    const [date, setDate] = useState(0);


    function input(){
        console.log(`The ${details} has been saved`);
    }

    return (

        <Card>
            <Card.Body>
                <Card.Title>{account}</Card.Title>
                <Card.Text>
                    <span className="subtitle">Details:</span>
                    <br />
                    {details}
                    <br />
                    <span className="subtitle">cost: </span>
                    PhP {cost}
                    <br />
                    <span className="subtitle">Date: </span>
                    {date}
                    <br />
                </Card.Text>

                <Button variant="primary" onClick={enroll}>Enroll</Button>
                
            </Card.Body>
        </Card>

    )
}

// Checks if the Course component is getting the correct prop types
Account.propTypes = {
    // shape() is used to check that a prop object conforms to a specific "shape" or data structure
    account: PropTypes.shape({
        //define the properties and their expected types
        account: PropTypes.string.isRequired,
        details: PropTypes.string.isRequired,
        cost: PropTypes.number.isRequired,
        date: PropTypes.string.isRequired,
    })
}