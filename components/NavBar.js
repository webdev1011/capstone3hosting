import React, { useContext, useEffect, useState } from 'react';
// Import nextJS Link component for client-side navigation
import Link from 'next/link';
// Import necessary bootstrap components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';
import AppHelper from '../app-helper';


export default function NavBar() {
    // Consume the UserContext and destructure it to access the user state from the context provider
    const { user } = useContext(UserContext);

    const [data, setData] = useState('');

    useEffect(() => {
            const options = {
                headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
            }

            fetch(`${ AppHelper.API_URL }/users/details`, options)
            .then(AppHelper.toJSON)
            .then((data) => {

                setData(data)
            })
        }, [])


    return (

        <Navbar bg="success" expand="lg">
            <Link href="/">
                <a className="navbar-brand"><i className="fa fa-envelope" aria-hidden="true"></i> Budget Tracker App</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                   
                    {(user.id !== null) ?
                        <React.Fragment>
                            <Link href="/records">
                                <a className="nav-link" role="button">
                                    Records
                                </a>
                            </Link>
                            <Link href="/userProfile">
                                <a className="nav-link" role="button">
                                    {data.firstName}
                                </a>
                            </Link>
                            <Link href="/logout">
                                <a className="nav-link" role="button">
                                    Logout
                                </a>
                            </Link>
                        </React.Fragment>
                        :
                        <React.Fragment>
                            <Link href="/login">
                                <a className="nav-link" role="button">
                                    Log in
                                </a>
                            </Link>
                            <Link href="/register">
                                <a className="nav-link" role="button">
                                    Sign up
                                </a>
                            </Link>
                        </React.Fragment>
                    }

                </Nav>
            </Navbar.Collapse>
        </Navbar>

    )
}