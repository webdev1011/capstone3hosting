import { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';


export default function ExpenseBarChart({ expenseRecords }){

	// console.log(recordsData);

	const [months, setMonths] = useState(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]);
	const [monthlyExpenses, setMonthlyExpenses] = useState([]);

	useEffect(() => {

		setMonthlyExpenses(months.map(month => {
		
				let expenses = 0;
	
				expenseRecords.forEach(element => {
	
					if(moment(element.createdOn).format('MMMM') === month){
						expenses += element.amount;
					}
	
				})
	
				return expenses;
	
			})
		)
	}, [expenseRecords])

	// console.log(monthlyExpenses)

	const data = {
		labels: months,
		datasets: [{
			label: 'Monthly Expenses for the year 2021',
			backgroundColor: 'rgba(255, 99, 132, 0.2)',
			borderColor: 'rgba(255, 99, 132, 1)',
			borderWidth: 1,
			hoverBackgroundColor: 'rgba(255, 99, 132, 0.4)',
			hoverBorderColor: 'rgba(255, 99, 132, 1)',
			data: monthlyExpenses
		}]
	}

	return(
		<Bar data={data}/>
	)
}