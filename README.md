## WDC028-28 to WDC028-29

### Code-along

1. Bootstrap a Next.js app via the terminal command:
> npx create-next-app appName

2. Navigate to the project root directory and run the development server via the terminal command:
> npm run dev

3. Next.JS has a built-in routing system that depends on the file name of pages to determine their routes. As such, pages/index.js is routed to the '/' base URI. Let's replicate our home page from the previous session.
> the api subdirectory within the pages directory is for defining API endpoints. Using this will make our application monolithic. We will not be using this as we want to practice a decoupled approach to development.

4. Create a components folder and within it, create the Banner and Highlights components replacing the Link from react-router-dom (no longer needed) with the Link from NextJS.

5. Modify the contents of the index.js file in the pages directory to replicate our home page from our React booking app.

6. Create a file named _app.js within the pages directory to serve as the entry point of our app where globally scoped objects may be imported.

7. Create a styles directory in the root directory of our project. In it, create a css file named global. Transfer our CSS from our React booking app here.

8. Import global.css in _app.js.

9. Import CSS Bootstrap in _app.js.

10. Copy UserContext.js together with the users and courses data from our React booking app. Replicate their file locations in our current project.

11. In the components folder, replicate our Navbar component from our React booking app.

12. Replicate our layout in the _app.js file.

13. Reimplement the UserContext provider in the _app.js file.

14. Replicate the Course component.

15. Create a courses subdirectory in the pages folder and within it replicate our Courses page as a file named index.js. Doing so will render that page when we access the /courses endpoint.

16. Replicate the Login and Logout components from our React booking app.

17. Replicate the conditional rendering of the Navbar now that user state can be changed globally.

### Activity
1. Have the students replicate the register page routed to its respective navbar link.

2. Have the students replicate the create course page routed to its respective navbar link.